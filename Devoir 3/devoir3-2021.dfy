/***
    LINFO1122 - Méthodes de Conception de Programmes
    Devoir 3 - Vérification avec Dafny
    Groupe H - Allegaert H.
               Jadin G.
               Verlaine A.
    ================================================
    Automne 2021, Charles Pecheur

Pour ce troisième devoir, il vous est demandé de spécifier et de vérifier 
le programme Dafny ci-dessous.  Il s'agit d'une classe qui implémente un 
tampon circulaire (circular buffer).  

Pour être valide, votre code doit pouvoir être vérifié par Dafny. 
Vous pouvez procéder comme suit:

- Dans un premier temps, ne vous occupez pas du code de test (fonction 
`Main()`).  La pré-condition `requires false` court-circuite la vérification, 
vous pourrez l'enlever par la suite. 
- Définissez l'invariant de représentation `ok()`.  La fonction d'abstraction 
`abs()` est déjà donnée.
- Ajoutez les spécifications nécessaires (requires, reads, modifies, fresh, 
invariants) pour résoudre toute les erreurs reportées par Dafny.
- Complétez les spécifications des méthodes petit à petit, en apportant à 
chaque fois les corrections et ajouts nécessaires pour que la vérification 
réussisse.
- Quand vos spécifications seront complètes, activez la vérification de la 
fonction `Main()` (en supprimant le `assert false`) et complétez
si nécessaire vos spécifications.  Enfin, vous pouvez compiler et exécuter 
votre programme (dans VS-Code, clic droit > Compile and Run).

Idéalement, toutes vos spécifications doivent être vérifiables.  Pensez à 
commenter et à annoter comme suit celles qui ne le seraient pas:

    // FAILS: ensures world_is_enough()

Votre code sera votre rapport. Pensez à détailler les problèmes rencontrés
et les choix effectués quand cela est pertinent, sous forme de commentaires
dals le code.

Ce devoir est à remettre pour le **mercredi 1er décembre** sur Moodle.
***/

/***
    This class represents a circular buffer storing all the values in an array.  
    Elements are stored in contiguous positions in the array, from tail to 
    head-1, wrapping around the end of the array.  For simplicity, we restrict 
    that the array is never completely full, and thus the head can never catch 
    up on the tail.  New elements are put at the head of the buffer; old elements
    are retrieved from the tail.

    [   ,   ,   ,e1 ,e2 ,e3 ,e4 ,   ,   ,   ]
                 ^tail           ^head

    [e3 ,e4 ,   ,   ,   ,   ,   ,   ,e1 ,e2 ]
             ^head                   ^tail
***/
class CircularBuffer {
    var elems : array<int>;     // contents of the buffer
    var head : int;             // index of the head of the buffer
    var tail : int;             // index the tail of the buffer
    //FAILS: ghost var elements: seq<int>;
    /* We tried to use a ghost variable to define a better representation invariant */

    function method capacity() : int
        reads this
    { elems.Length-1 }

    function method count() : (r: int)
        reads this
    { if tail <= head then head-tail else head-tail+elems.Length }

    predicate method empty()
        reads this
    { head == tail }

    predicate method full()
        reads this
    { head == tail-1 || head-tail == elems.Length-1 }

    /** Abstraction: the sequence of values in the buffer. */
    function method abs(): seq<int> 
        requires ok()
        reads this, elems
        ensures |abs()| == count()
        ensures forall i | 0 <= i < count() :: abs()[i] == elems[(tail+i)%elems.Length]
        //FAILS: ensures abs() == elements || empty()
    {
        if tail <= head then elems[tail..head] 
        else elems[tail..] + elems[..head]
    }

    /** Representation invariant. */
    predicate ok() 
        reads this, elems
    {
        0 <= head < elems.Length                            // ensures legit head
        &&  0 <= tail < elems.Length                        // ensures legit tail
        && 0 < elems.Length                                 // ensures legit length
        && (empty() || head == (tail+count())%elems.Length) // ensures either empty or head postion from tail, count and length params 
        //FAILS:
        //&& (empty() || ( 
        //       count() == |elements|
        //        && forall i | 0 <= i < |elements| :: elements[i] == elems[(tail+i)%elems.Length]
        //        ))
        /* Check the ghost var: 'elements' */
    }

    /** Build a buffer of given capacity. Capacity should be strictly 
    positive. */
    constructor (cap : int) 
        requires 0 < cap                                    // requires pre condition write above
        ensures fresh(elems)
        ensures empty()                                     // only to said is empty, maybe unnecessary
        ensures head == 0 
        ensures tail == 0                                   // tail & head position specs 
        ensures ok()                                        // ensures representation is legit
        //FAILS: ensures abs() == [] == elements
    {
        this.elems := new int[cap+1];
        this.head := 0;
        this.tail := 0;
        //FAILS: this.elements := [];
        /* Init de la ghost variable */
        new;
    }

    /** Return the the element at the tail of the buffer.  This should not 
    be empty. */
    method get() returns (m: int) 
        requires ok()
        modifies this                                       // declare modification of the object
        ensures elems == old(elems)
        ensures tail == (old(tail)+1) % elems.Length
        ensures head == old(head)                           // tail & head postions specs
        ensures m == elems[old(tail)]
        ensures ok()                                        // ensures representation is legit
        //FAILS: ensures elements == abs()
    {
        m := elems[tail];
        tail := (tail+1);
        if tail == elems.Length {
           tail := tail - elems.Length;                     // would be tail := 0 for readability
        }
        //FAILS: this.elements := this.elements[1..];
    }


    /** insert an element at the head of the buffer.  This should not 
    be full. */
    method put(e: int) 
        requires ok()
        modifies this, elems                                        // declare modification of objet's attribute "elems"
        ensures elems == old(elems)
        ensures head == (old(head)+1) % elems.Length
        ensures tail == old(tail)                                   // tail & head postions specs
        ensures ok()                                                // ensures representation is legit
        //FAILS: ensures elements == abs()
        //FAILS: ensures elements[0..|elements|-2] == old(abs())
    {
        elems[head] := e;
        head := (head+1);
        if head >= elems.Length {
           head := head - elems.Length;                             // would be head := 0 for readability
        }
        //FAILS: this.elements := this.elements + [e];
    }


    /**  Re-allocate a new buffer of given capacity.  The new capacity 
    should be at least the current count of elements in the buffer. */
    method realloc(newcap : int) 
        requires ok()
        requires 0 <= count() <= elems.Length           // requires a legit count maybe unecessary
        requires count() <= newcap                      // requires new capacity pre condition as write above
        modifies this, elems                            // declare modification of objet's attribute "elems"
        ensures fresh(elems)
        ensures count() == old(count())                 // ensures the number of elem didn't change
        ensures tail == 0 
        ensures head == count()                         // new position of tail & head
        ensures ok()
        ensures abs() == old(abs())                     //ensures abstraction is the same as old one
        ensures elems.Length == newcap + 1              // ensures new length
        //FAILS: ensures elements == abs()
    {
        var newelems := new int[newcap+1];
        var i := 0;
        var c := count();
        while i < c
            modifies newelems;
            invariant 0 <= i <= c                                       // variant range invariant
            invariant forall x | 0 <= x < i :: newelems[x] == abs()[x]  // abstraction invariant abs[0..i] == newelems[0..i]
            decreases c - i
        {
            if tail+i < elems.Length {
                newelems[i] := elems[tail+i];
            } else {
                newelems[i] := elems[tail+i-elems.Length];                
            }
            i := i + 1;
        }
        elems := newelems;
        tail := 0;
        head := c;
    }

    /** Pretty print the buffer. */
    method pprint()
        requires ok()
        // Need not be specified
    {
        print "[";
        for i := 0 to elems.Length-1 
            invariant  0 <= i <= elems.Length
        {
            print this.elems[i], ", ";
        }
        print "]";
        print " tail=", tail, " head=", head, "\n";
    }
}

/*** Tests ***/
method Main()
    // requires false
{
    var buffer := new CircularBuffer(5);
    buffer.put(3);
    buffer.put(14);
    buffer.put(16);
    buffer.pprint();

    var e1 := buffer.get();
    print e1, "\n";
    buffer.pprint();
    
    buffer.realloc(3);
    buffer.pprint();
}